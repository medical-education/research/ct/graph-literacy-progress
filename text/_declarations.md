
### Ethics approval and consent to participate

Approval was obtained from the local Ethics Committee University Muenster, Germany. A validated questionnaire for "Graph Literacy" was handed out to medical students during April 2017 to June 2017. Completion of the questionnaire was voluntary.

### Consent for publication

Not applicable

### Availability of data and materials

The original data that support the findings of this study are available from Open Science Framework (osf.io, see manuscript-URL).

### Competing interests

The authors declare that they have no competing interests.

### Funding

The author(s) received no specific funding for this work.

### Authors' contributions
We thank Mirta Galesic and Rocio Garcia-Retamero for providing their Graph Literacy scale (pen and paper format). 

### CRediT authorship contribution statement

**Janina Soler Wenglein:** Data curation, Formal analysis, Investigation, Methodology, Visualization, Writing - original draft.  
**Hendrik Friederichs:** Conceptualization, Formal analysis, Investigation, Methodology, Supervision, Writing - review & editing, Writing - original draft.

### Acknowledgments

The authors are grateful for the insightful comments offered by the anonymous peer reviewers at {{< meta citation.container-title >}}. The generosity and expertise of one and all have improved this study in innumerable ways and saved us from many errors; those that inevitably remain are entirely our own responsibility.