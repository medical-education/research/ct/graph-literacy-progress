
plain-language-summary: |
  **Relevantes Problem:** Gesundheitskompetenz beruht auf verschiedenen Fähigkeiten zur Informationsverarbeitung. Sie ist für Ärzte essentiell, um medizinische Berichte, Behandlungen und Studienergebnisse zu verstehen, Patienten aufzuklären, klinische Entscheidungen zu treffen und effektiv im medizinischen Team zu kommunizieren.
  
  **Fokussiertes Problem:** Graph Literacy, als Teil der Gesundheitskompetenz, ist wichtig für das Lesen und Verstehen von Grafiken. Es umfasst das Entziffern von Zeichen und Symbolen und ist eng mit anderen Formen der Literacy verbunden. Die Annahme grafische Darstellungen würden grundsätzlich zum vereinfachten Verständnis von Informationen beitragen, ist irreführend. Dies ist besonders relevant in Bereichen wie der medizinischen Forschung, wo Grafiken zur Vermittlung komplexer Informationen genutzt werden. Fehlerhafte rezipierte Darstellungen können ebenso wie Fehldarstellungen die Informationsaufnahme beeinflussen. 
  
  **Gap des Problems:** 
  <!-- Gap / Dilemma / Widerspruch subj. Erwartung und Realität -->
  Graph Literacy beeinflusst das Risikoverständnis und Entscheidungsverhalten, wobei Studien hauptsächlich Patienten und Ärzte im Umgang mit grafischen Darstellungen betrachten. Zur Graph Literacy bei Medizinstudierenden sind bislang wenig Forschungsergebnisse publiziert, so dass auch ein tieferes Verständnis davon, wie die Graph Literacy im Medizinstudium gestärkt werden kann, fehlt. 
  
  **Lösung?:** 
  <!-- möglicher Fortschritt / mögliche Lösung - Ansprechen von Motiv(en): Anschluss, Leistung, Macht -->
  Für Ärzte, die Menschen mit geringerer Gesundheitskompetenz betreuen, ist es wichtig, selbst eine hohe Kompetenz im Verständnis visueller Darstellungen zu haben. Diese Fertigkeit sollte schon im Medizinstudium erworben werden.

  **Forschungsfragen:** 
  Daher wurde eine Studie mit Medizinstudierenden durchgeführt, um ihre Fähigkeit zur Interpretation visuell dargestellter medizinischer Informationen zu untersuchen.  
  
  **Studienpopulation:** Medizinstudierende
  
  **Studiendesign:** Beobachtungsstudie  
  
  **Datenerhebung:**  Graph Literacy Scale  
  
  **Ergebnisparameter:**  Anzahl der richtigen Antworten insgesamt.
  
  **Statistik:**  Bestimmung der Prozentwerte für die absolute und z-Scores und Perzentilen für die relative Bewertung der Leistungen im Vergleich der Kohorten unterschiedlicher Studienjahre.




