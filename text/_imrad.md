## Background

### Broad problem

Health literacy depends on diverse aspects of skills in processing information. To adequately understand medical reports, treatments and study results a set of abilities is needed. Thinking of future physicians one can imagine a multitude of situations where a high health literacy is required: whenever talking with patients about medical data, consenting in treatments and educating patients about diseases, making clinical decisions depending on laboratory results, imaging and study results, understanding evidence, interpreting of epidemiological data and communicating in medical teams.

### Theoretical and/or empirical focus of the problem

One important aspect of health literacy is graph literacy, meaning the reading, interpreting and understanding of graphs. This process is depending on decoding and interpreting signs and symbols and is known as semiotic activity. When informations are provided graphically the underlying assumption, this could reduce the cognitive work load and would be easier to understand, seems obvious. Thus, the ability to understand graphs should not be considered isolated from other forms of literacy. It is an integral part of the ability to process and communicate information effectively in a world that is increasingly dependent on data and its visual representation.

Processing those visual representations is essential for understanding scientific and statistical data [@friel2001making] and particularly relevant in areas such as medical research where graphs and data visualizations are frequently used to convey complex information. A personal understanding of the representations is essential when preparing data for communication in order to ensure adequate knowledge transfer to others [@cooper2002graphical]. But misleading representations (either through deliberate manipulation or unintentionally through errors or incompleteness) can also have a significant influence on the reception of information by the recipient [@melnik2023my].

In summary it ca be said that graph literacy, as a form of semiotic activity, is a crucial component of overall literacy [@roth2002reading]. It can have an impact on risk comprehension [@okan2019using], suggesting that a higher graph literacy may be associated with a better decision-making performance. However, studies of graph literacy mainly refer to patients [@durand2020graph] or the ability of doctors [@caverly2015doctors] to interpret graphical representations. Data regarding medical students ability of graph literacy and investigations on how to increase these are lacking. 

### Focused problem statement

Especially for those advising and informing people with less health and graph literacy, it is important to achieve high competence in graph literacy themselves. But lack of understanding of visual representations can significantly impact decision making for patients and for (future) medical doctors.

When providing information to patients, medical doctors must be aware about patient health literacy and about their own. Therefore, we conducted a cohort study with medical students for understanding their ability to interpret medical information provided visually.

### Statement of study intent

We performed a study of medical students to investigate the following questions:

1.  How high is the graph literacy among medical students?
2.  Does the graph literacy of medical students change during their respective academic progress (measured in years)?
3.  Are there any sub-areas of medical students' graph literacy that require special attention?

## Methods

### Setting and subjects

Our cross-sectional study was performed with medical students in their academic years 1, 2, 3, 4 and 5 at the medical school of the University of Muenster, Germany. It takes six years to complete a course in medical school in Germany, with students enrolled directly from secondary schools. The course of study is divided into a pre-clinical section (the first two years) and a clinical section (the last four years). To improve students' clinical experience, they are rotated in various hospital departments during their final year ("clinical/practical" year).

### Study design

Graph literacy can be measured with the so-called 'graph literacy scale' introduced by Galesic et al. in 2011 [@galesic2011]. The students were asked to participate in the study and answer the questions of the "Graph Literacy Scale" (Galesic and Garcia-Retamero, 2011) with pen and paper. The validated questionnaire was handed out during April 2017 to June 2017. Completion of the questionnaire was voluntary, anonymously and performed in the context of the progress test. A mandatory examination, taking place annually at the medical faculty. No calculators were allowed. We did not ask participants about their social, biographical or educational background, but a post-secondary school diploma (abitur) is necessary for entering medical school and a statistical curriculum is part of the school education in the German upper school.

### Ethical approval

Approval was obtained from the local Ethics Committee University Muenster, Germany.

### Data collection

Data collection for this study was determined à priori as follows: students of each study year of the medical University Muenster are asked to voluntarily participate and answer questions of the "Graph Literacy Scale". The study takes place during the annual progress test, a obligatory test students of all study years have to take. 

### Outcome Measures

We did measure outcome using the “Graph Literacy Scale” (Galesic and Garcia-Retamero, 2011). This test measures the graph literacy, meaning to read and interpret graphical provided information. The students had to answer 9 open and 4 multiple choice questions. Each of the questions represents one of the 3 levels of graph comprehension: reading the data, reading between the data, reading beyond the data (Galesic and Garcia-Retamero, 2011). Except for question 7 there is only one correct answer to every question. Question 7 does accept a range (\>23 to \>25) of correct answers. Participants received one point for every correct solution, meaning they could achieve 0 to 13 points in total. Only the correct answer was taken into account and only the defined exact numbers were considered correct in open questions. There was no error margin, questionnaires including blank responses were not considered for our analysis. The average score (standard
deviation) in this test format according to Galesic and Garcia-Retamero (2011) is 9.4 (standard deviation 2.6) for Germany. 

### Statistical methods

All data were entered and alyzed using the statistial software R Studio 2023.12.1. Descriptive means and standard deviations were calculated for participants' age and mean test scores were computed for all participants' and for each study year seperately. 

```{webr-r}
#| context: interactive

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
) # Download the dataset

# Read the data
penguins = read.csv("penguins.csv")

# Scatterplot example: penguin bill length versus bill depth
ggplot2::ggplot(data = penguins, ggplot2::aes(x = bill_length_mm, y = bill_depth_mm)) + # Build a scatterplot
  ggplot2::geom_point(ggplot2::aes(color = species, 
                 shape = species),
             size = 2)  +
  ggplot2::scale_color_manual(values = c("darkorange","darkorchid","cyan4"))
```


## Results

### Recruitment Process and Demographic Characteristics

We obtained 449 complete questionnaires, which could be included in our analysis. Out of a total of 500 questionnaires 31 surveys were actively canceled by the respondents and therefore not considered. Out of those 469 left over, 20 were lacking answers regarding individual responses and therefor haven’t been included. 2 questionnaires were lacking information about the sex of the respondents, 1 was lacking information regarding age, those 3 weren’t excluded. Out of our 449 included questionnaires 126 students were in their first academic year, 128 students in their second academic year, 54 students in their third year, 59 students in their fourth year and 82 students in their fifth year. 151 students stated their sex was male, 296 stated their sex was female, 2 didn't give informations regarding sex.

### Primary and secondary Outcomes

After the evaluation of all datasets, three major findings were obtained. The first finding is that participants achieved an average score of 11.42 (s = 1.42) points, which is significantly higher than the average score of Galesic (2011) with 9.4 (s = 2.6) points. The second major finding is, although there is a slight increase in the scores between the first (11.33 points [s = 1.40]) to the fifth academic year (11.65 [s = 1.18]), no significant difference could be detected (p = xx). Finally, although the students had an excellent overall performance, there was one question (question 11, testing abilities to read beyond the data) achieving poor results with an increase of correct answers in the older cohorts, but still a ceiling effect on a low level with a maximum of 57,6% correct answers. 

## Discussion

The first major finding of our study is that medical students showed above average test results in the “Graph Literacy Scale” in terms of their graph literacy compared to a German standard population. Although abilities in reading the data and between the data are high in our cohorts, abilities to read beyond the data are inconsistent. Fluctuation range in the area of reading beyond the data is extremely high between individual questions. A finding also seen in the validation study (Galesic, 2011). Although our medical students perform better than the reference group, the overall result for question 11 is poor with 38.1%-57.3% (mean 48,3%) correct answers. Although there is an improvement in the ability to answer the question across the different cohorts with each year of study, there is a certain ceiling effect. Question 11 requires the comparison between 2 line charts. The participants are asked to decide based on those line charts which treatment contributes to a larger decrease of sick patients. The y-axes in those charts state "% sick patients" and does not give informations regarding the scale. The correct answer would be "can't say" based on the charts. @Garcia2011

Scoring high in other aspects of reading beyond the data, a possible explanation for poor performance of our medical students could be a base rate neglect manifesting with poor results when answering question 11. Whenever the correct answer is “can’t say” also a lacking tolerance of ambiguity could be an explanation for a poor performance. Considering the role of physicians giving answers and guiding therapy, admitting one can’t give an answer, might be especially complex. Low aversion to ambiguity is a known risk factor for suboptimal decision-making in clinical practice, as for example is shown by @Saposnik2011 for treatment in patients with Spinal Muscular Atrophy (SMA). If the assumption that a lack of ambiguity tolerance contributes to the response behavior is correct, this has far-reaching consequences for the need for training of the low graph literacy shown in our medical students cohort.

There are some limitations to our study, that need to be acknowledged. First, we performed a cross-sectional study, that was carried out on a specific point in time and in the setting of an mandatory test, every medical student has to take. This might have caused students to care less for correct answers or having a lower attention span because they already had to spend severe effort to perform well in their test. We can't say, if participants might have performed much better at a different time or at a later stage of their training, for example as residents in clinical practice. There might be an training effect for graph literacy, that only evoles over time needing to read and explain data regularly to patients or colleagues. Further we didn't ask participants to give specific informations regarding their statistical training in school or during their medical studies. We also can't say why the performance regarding graph literacy was especially inconsistent in "reading beyond the data". This question does need further research attention.
Nonetheless, a cross-sectional study design remains an efficient way to evaluate the prevalence of certain skills in a large sample of students. A true statement about how the graph literacy changes can not be given, because we did a single time measurement. But we can say, that in comparisson those students of higher academic experience do perform better when measuring their graph literacy.

### Integration with prior work

Only a few studies provide insights into the graphical and numerical skills among medical students.

In a cross-sectional, descriptive study, the researchers applied the Objective Numeracy, Subjective Numeracy, and Graph Literacy Scales to medical students in their final two years of medical school and to medical residents. The study included 169 participants, comprising 70% sixth-year seventh-year students, and 30% residents. The findings showed that the mean graph literacy was 10.35. A multiple linear regression analysis revealed that higher scores in the Graph Literacy Scale were associated with the male gender and younger age. The study concluded that numeracy and graph literacy scales' mean scores were high among the medical students in this sample [@mas2018graphical]. This is consistent with our research findings, which also revealed high overall graph literacy skills for medical students. Due to the very good results of the students at the level of data reading skills, the authors of the study recommend a reduction of the graph literacy measurement for medical students to the aspects of reading between and beyond the data. Assuming this could lead to a more objective measurement of graphical skills in trained persons. However, since only a precise record of all aspects of the abilities and weaknesses of medical students provides a comprehensive picture, this abbreviation does not appear to be adequate for research purposes.

Literature regarding patients' and nurses' graphy literacy does provide further insight with respect to the different levels of graph literacy. For example Durand et al @Durand2020 examine the relationship between graph literacy, numeracy, health literacy, and sociodemographic characteristics in a Medicai-eligible population. They did not find a statistically significant association betwwen Graph literacy and higher education. But they found a positive relationship between graph literacy and numeracy, while numeracy was associated to education. @Gaissmaier2012 emphasizes the significance of graph literacy in understanding health-related statistical information and ability for informed medical decision making. They found a high graph literacy to be associated with a better comprehension and recall for graphics instead of numbers. This makes graph literacy tremendous important in patient-centered communication tools, as shown by @Nayak2016. @Dowding2018 do support this statement for visual displays for clinical dashboards used by nurses. 

@Gaissmeier2023 people fail to identify what they comprehend better but base their choice between representations on how attractive the representation is.

@Tolbert2018 discusses the impact of different graphical representations on the accurate interpretation and clarity of patient-reported outcome scores in cancer patients/survivors.

### Implications for practice

Despite some improvement across different cohorts with each year of study, there seems to be a ceiling effect in abilities for reading beyond the data. This suggests that even with progression through medical education, there are limitations in the learning ability for this particular item. A possible base rate neglect and a lacking tolerance of ambiguity as explanations for poor performance, particularly when the correct answer is "can't say," adds a psychological dimension to the interpretation of our results. The reluctance to admit uncertainty, especially in a medical context where physicians are expected to provide answers and guide therapy, could be contributing to this specific challenge.
For future training of medical students, addressing these challenges could involve interventions to improve tolerance for ambiguity and emphasize the importance of acknowledging uncertainty when necessary. Training programs might incorporate exercises that specifically target those skills. Additionally, providing awareness and education about base rate neglect could enhance decision-making in situations where uncertainty is inherent.
Further it can be deduced that special attention in graph literacy of medical students should also be paid to the ability to communicate visually presented results. This takes into account the doctor as a mediator between specialist knowledge and patient interests, as well as the possibly lower graph literacy skills of patients.

### Implications for research

Our findings suggest that while medical students generally exhibit above-average results in graph literacy, particularly in reading and interpreting data, there are challenges in their ability to go beyond the presented data. This fluctuations in performance does indicate a need for future research. Exploring the psychological factors influencing graph literacy in medical students, such as tolerance of ambiguity, could be valuable to gain a better understanding. Investigating interventions that could improve performance in reading beyond the data, especially in situations involving uncertainty, could contribute to the development of more effective educational strategies.

### Conclusions

In summary, our findings suggest a nuanced understanding of the strengths and challenges in the graph literacy of medical students, with implications for both their training and need of further research in this domain.

### Acknowledgements
We thank Mirta Galesic and Rocio Garcia-Retamero for providing their Graph Literacy scale (pen and paper format). This study had no external funding. 

