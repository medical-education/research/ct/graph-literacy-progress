// Some definitions presupposed by pandoc's typst output.
#let blockquote(body) = [
  #set text( size: 0.92em )
  #block(inset: (left: 1.5em, top: 0.2em, bottom: 0.2em))[#body]
]

#let horizontalrule = [
  #line(start: (25%,0%), end: (75%,0%))
]

#let endnote(num, contents) = [
  #stack(dir: ltr, spacing: 3pt, super[#num], contents)
]

#show terms: it => {
  it.children
    .map(child => [
      #strong[#child.term]
      #block(inset: (left: 1.5em, top: -0.4em))[#child.description]
      ])
    .join()
}

// Some quarto-specific definitions.

#show raw.where(block: true): block.with(
    fill: luma(230), 
    width: 100%, 
    inset: 8pt, 
    radius: 2pt
  )

#let block_with_new_content(old_block, new_content) = {
  let d = (:)
  let fields = old_block.fields()
  fields.remove("body")
  if fields.at("below", default: none) != none {
    // TODO: this is a hack because below is a "synthesized element"
    // according to the experts in the typst discord...
    fields.below = fields.below.amount
  }
  return block.with(..fields)(new_content)
}

#let empty(v) = {
  if type(v) == "string" {
    // two dollar signs here because we're technically inside
    // a Pandoc template :grimace:
    v.matches(regex("^\\s*$")).at(0, default: none) != none
  } else if type(v) == "content" {
    if v.at("text", default: none) != none {
      return empty(v.text)
    }
    for child in v.at("children", default: ()) {
      if not empty(child) {
        return false
      }
    }
    return true
  }

}

#show figure: it => {
  if type(it.kind) != "string" {
    return it
  }
  let kind_match = it.kind.matches(regex("^quarto-callout-(.*)")).at(0, default: none)
  if kind_match == none {
    return it
  }
  let kind = kind_match.captures.at(0, default: "other")
  kind = upper(kind.first()) + kind.slice(1)
  // now we pull apart the callout and reassemble it with the crossref name and counter

  // when we cleanup pandoc's emitted code to avoid spaces this will have to change
  let old_callout = it.body.children.at(1).body.children.at(1)
  let old_title_block = old_callout.body.children.at(0)
  let old_title = old_title_block.body.body.children.at(2)

  // TODO use custom separator if available
  let new_title = if empty(old_title) {
    [#kind #it.counter.display()]
  } else {
    [#kind #it.counter.display(): #old_title]
  }

  let new_title_block = block_with_new_content(
    old_title_block, 
    block_with_new_content(
      old_title_block.body, 
      old_title_block.body.body.children.at(0) +
      old_title_block.body.body.children.at(1) +
      new_title))

  block_with_new_content(old_callout,
    new_title_block +
    old_callout.body.children.at(1))
}

#show ref: it => locate(loc => {
  let target = query(it.target, loc).first()
  if it.at("supplement", default: none) == none {
    it
    return
  }

  let sup = it.supplement.text.matches(regex("^45127368-afa1-446a-820f-fc64c546b2c5%(.*)")).at(0, default: none)
  if sup != none {
    let parent_id = sup.captures.first()
    let parent_figure = query(label(parent_id), loc).first()
    let parent_location = parent_figure.location()

    let counters = numbering(
      parent_figure.at("numbering"), 
      ..parent_figure.at("counter").at(parent_location))
      
    let subcounter = numbering(
      target.at("numbering"),
      ..target.at("counter").at(target.location()))
    
    // NOTE there's a nonbreaking space in the block below
    link(target.location(), [#parent_figure.at("supplement") #counters#subcounter])
  } else {
    it
  }
})

// 2023-10-09: #fa-icon("fa-info") is not working, so we'll eval "#fa-info()" instead
#let callout(body: [], title: "Callout", background_color: rgb("#dddddd"), icon: none, icon_color: black) = {
  block(
    breakable: false, 
    fill: background_color, 
    stroke: (paint: icon_color, thickness: 0.5pt, cap: "round"), 
    width: 100%, 
    radius: 2pt,
    block(
      inset: 1pt,
      width: 100%, 
      below: 0pt, 
      block(
        fill: background_color, 
        width: 100%, 
        inset: 8pt)[#text(icon_color, weight: 900)[#icon] #title]) +
      block(
        inset: 1pt, 
        width: 100%, 
        block(fill: white, width: 100%, inset: 8pt, body)))
}



#let article(
  title: none,
  authors: none,
  date: none,
  abstract: none,
  cols: 1,
  margin: (x: 1.25in, y: 1.25in),
  paper: "us-letter",
  lang: "en",
  region: "US",
  font: (),
  fontsize: 11pt,
  sectionnumbering: none,
  toc: false,
  toc_title: none,
  toc_depth: none,
  doc,
) = {
  set page(
    paper: paper,
    margin: margin,
    numbering: "1",
  )
  set par(justify: true)
  set text(lang: lang,
           region: region,
           font: font,
           size: fontsize)
  set heading(numbering: sectionnumbering)

  if title != none {
    align(center)[#block(inset: 2em)[
      #text(weight: "bold", size: 1.5em)[#title]
    ]]
  }

  if authors != none {
    let count = authors.len()
    let ncols = calc.min(count, 3)
    grid(
      columns: (1fr,) * ncols,
      row-gutter: 1.5em,
      ..authors.map(author =>
          align(center)[
            #author.name \
            #author.affiliation \
            #author.email
          ]
      )
    )
  }

  if date != none {
    align(center)[#block(inset: 1em)[
      #date
    ]]
  }

  if abstract != none {
    block(inset: 2em)[
    #text(weight: "semibold")[Abstract] #h(1em) #abstract
    ]
  }

  if toc {
    let title = if toc_title == none {
      auto
    } else {
      toc_title
    }
    block(above: 0em, below: 2em)[
    #outline(
      title: toc_title,
      depth: toc_depth
    );
    ]
  }

  if cols == 1 {
    doc
  } else {
    columns(cols, doc)
  }
}
#show: doc => article(
  title: [Assessment of graph literacy among German medical students – a cross-sectional study to assess graph interpretation skills],
  authors: (
    ( name: [Janina Soler Wenglein],
      affiliation: [Universität Bielefeld, Medizinische Fakultät OWL],
      email: [janina.soler\@uni-bielefeld.de] ),
    ( name: [Hendrik Friederichs],
      affiliation: [Universität Bielefeld, Medizinische Fakultät OWL],
      email: [hendrik.friederichs\@uni-bielefeld.de] ),
    ),
  date: [2024-02-14],
  lang: "en",
  abstract: [#strong[Background];: Health literacy depends on diverse aspects of skills in processing information. To adequately understand medical reports, treatments and study results a set of abilities is needed, including understanding graphs properly. Especially for those advising and informing people with less health and graph literacy, it is important to achieve high competence in graph literacy themselves. When providing information to patients graph literacy is a form of semiotic activity that is a crucial component of overall literacy for \(future) medical doctors.

#strong[Methods];: A cross-sectional study was performed with medical students in their academic years 1, 2, 3, 4 and 5 at the medical school of the University of Muenster, Germany. We did measure outcome using the "Graph Literacy Scale" \(Galesic and Garcia-Retamero, 2011). This test measures the graph literacy, meaning to read and interpret graphical provided information.

#strong[Results];: Etiam maximus accumsan gravida. Maecenas at nunc dignissim, euismod enim ac, bibendum ipsum. Maecenas vehicula velit in nisl aliquet ultricies. Nam eget massa interdum, maximus arcu vel, pretium erat. Maecenas sit amet tempor purus, vitae aliquet nunc. Vivamus cursus urna velit, eleifend dictum magna laoreet ut. Duis eu erat mollis, blandit magna id, tincidunt ipsum. Integer massa nibh, commodo eu ex vel, venenatis efficitur ligula. Integer convallis lacus elit, maximus eleifend lacus ornare ac. Vestibulum scelerisque viverra urna id lacinia. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean eget enim at diam bibendum tincidunt eu non purus. Nullam id magna ultrices, sodales metus viverra, tempus turpis.

#strong[Conclusio];: The first major finding of our study is that medical students showed above average test results in the "Graph Literacy Scale" in terms of their graph literacy compared to a German standard population. Although abilities in reading the data and between the data are high in our cohorts, abilities to read beyond the data are inconsistent.

],
  toc_title: [Table of contents],
  toc_depth: 3,
  cols: 1,
  doc,
)
#import "@preview/fontawesome:0.1.0": *


#super[1] Universität Bielefeld, Medizinische Fakultät OWL

#super[✉] Correspondence: #link("mailto:hendrik.friederichs@uni-bielefeld.de")[Hendrik Friederichs \<hendrik.friederichs\@uni-bielefeld.de\>]

#block[
#callout(
body: 
[
This manuscript is a work in progress. However, thank you for your interest. Please feel free to visit this web site again at a later date.

#emph[Dieses Manuskript ist noch in Arbeit. Wir danken Ihnen jedoch für Ihr Interesse. Bitte besuchen Sie diese Website zu einem späteren Zeitpunkt noch einmal …]

]
, 
title: 
[
IN PROGRESS …
]
, 
background_color: 
rgb("#ffe5d0")
, 
icon_color: 
rgb("#FC5300")
, 
icon: 
fa-fire()
, 
)
]
#block[
#callout(
body: 
[
#strong[Relevantes Problem:] Gesundheitskompetenz beruht auf verschiedenen Fähigkeiten zur Informationsverarbeitung. Sie ist für Ärzte essentiell, um medizinische Berichte, Behandlungen und Studienergebnisse zu verstehen, Patienten aufzuklären, klinische Entscheidungen zu treffen und effektiv im medizinischen Team zu kommunizieren. \
#strong[Fokussiertes Problem:] Graph Literacy, als Teil der Gesundheitskompetenz, ist wichtig für das Lesen und Verstehen von Grafiken. Es umfasst das Entziffern von Zeichen und Symbolen und ist eng mit anderen Formen der Literacy verbunden. Dies ist besonders relevant in Bereichen wie der medizinischen Forschung, wo Grafiken zur Vermittlung komplexer Informationen genutzt werden. Fehldarstellungen können jedoch die Informationsaufnahme beeinflussen. \
#strong[Gap des Problems:] Graph Literacy beeinflusst das Risikoverständnis und Entscheidungsverhalten, wobei Studien hauptsächlich Patienten und Ärzte im Umgang mit grafischen Darstellungen betrachten. \
#strong[Lösung?:] Für Ärzte, die Menschen mit geringerer Gesundheitskompetenz betreuen, ist es wichtig, selbst eine hohe Kompetenz im Verständnis visueller Darstellungen zu haben. Diese Fertigkeit sollte schon im Medizinstudium erworben werden. \
#strong[Forschungsfragen:] Daher wurde eine Studie mit Medizinstudierenden durchgeführt, um ihre Fähigkeit zur Interpretation visuell dargestellter medizinischer Informationen zu untersuchen. \
#strong[Studienpopulation:] Medizinstudierende \
#strong[Studiendesign:] Beobachtungsstudie \
#strong[Datenerhebung:] Graph Literacy Scale \
#strong[Ergebnisparameter:] Anzahl der richtigen Antworten insgesamt. \
#strong[Statistik:] Bestimmung der Prozentwerte für die absolute und z-Scores und Percentilen für die relative Bewertung der Leistungen im Studienverlauf. Evtl. noch weitere Vergleichsberechnungen.

]
, 
title: 
[
STRUKTUR DES MANUSKRIPTS
]
, 
background_color: 
rgb("#ccf1e3")
, 
icon_color: 
rgb("#00A047")
, 
icon: 
fa-lightbulb()
, 
)
]
== Background
<background>
=== Broad problem
<broad-problem>
Health literacy depends on diverse aspects of skills in processing information. To adequately understand medical reports, treatments and study results a set of abilities is needed. Thinking of future physicians one can imagine a multitude of situations where a high health literacy is required: whenever talking with patients about medical data, consenting in treatments and educating patients about diseases, making clinical decisions depending on laboratory results, imaging and study results, understanding evidence, interpretation of epidemiological data and communication in medical teams.

=== Theoretical and/or empirical focus of the problem
<theoretical-andor-empirical-focus-of-the-problem>
One important aspect of health literacy is graph literacy, meaning the reading and understanding of graphs. This process is depending on decoding and interpreting signs and symbols and known as semiotic activity. Thus, the ability to understand graphs should not be considered isolatioted from other forms of literacy. It is an integral part of the ability to process and communicate information effectively in a world that is increasingly dependent on data and its visual representation.

Processing those visual representations is essential for understanding scientific and statistical data \[1\] and particularly relevant in areas such as medical research where graphs and data visualizations are frequently used to convey complex information. A personal understanding of the representations is essential when preparing data for communication in order to ensure adequate knowledge transfer to others \[2\]. But misleading representations \(either through deliberate manipulation or unintentionally through errors or incompleteness) can also have a significant influence on the reception of information by the recipient \[3\].

In summary it ca be said that graph literacy, as a form of semiotic activity, is a crucial component of overall literacy \[4\]. It can have an impact on risk comprehension \[5\], suggesting that a higher graph literacy may be associated with a better decision-making performance. However, studies of graph literacy mainly refer to patients \[6\] or the ability of doctors \[7\] to interpret graphical representations.

=== Focused problem statement
<focused-problem-statement>
Especially for those advising and informing people with less health and graph literacy, it is important to achieve high competence in graph literacy themselves. But lack of understanding of visual representations can significantly impact decision making for patients and for \(future) medical doctors.

When providing information to patients, medical doctors must be aware about patient health literacy and about their own. Therefore, we conducted a cohort study with medical students for understanding their ability to interpret medical information provided visually.

=== Statement of study intent
<statement-of-study-intent>
We performed a study of medical students to investigate the following questions:

+ What is …
+ Why are …

== Methods
<methods>
=== Setting and subjects
<setting-and-subjects>
Our cross-sectional study was performed with medical students in their academic years 1, 2, 3, 4 and 5 at the medical school of the University of Muenster, Germany. It takes six years to complete a course in medical school in Germany, with students enrolled directly from secondary schools. The course of study is divided into a pre-clinical section \(the first two years) and a clinical section \(the last four years). To improve students’ clinical experience, they are rotated in various hospital departments during their final year \("clinical/practical" year).

=== Study design
<study-design>
Graph literacy can be measured with the so-called 'graph literacy scale' introduced by Galesic et al.~in 2011 \[8\]. The students were asked to participate in the study and answer the questions of the "Graph Literacy Scale" \(Galesic and Garcia-Retamero, 2011) with pen and paper. The validated questionnaire was handed out during April 2017 to June 2017. Completion of the questionnaire was voluntary, anonymously and performed in the context of the progress test. A mandatory examination, taking place annually at the medical faculty. No calculators were allowed. We did not ask participants about their social, biographical or educational background, but a post-secondary school diploma \(abitur) is necessary for entering medical school and a statistical curriculum is part of the school education in the German upper school.

=== Ethical approval
<ethical-approval>
Approval was obtained from the local Ethics Committee University Muenster, Germany.

=== Data collection
<data-collection>
Data collection for this study was determined à priori as follows:

…

```{webr-r}
#| context: setup

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
)

# Read the data
df_penguins = read.csv("penguins.csv")
```

=== Outcome Measures
<outcome-measures>
We did measure outcome using the "Graph Literacy Scale" \(Galesic and Garcia-Retamero, 2011). This test measures the graph literacy, meaning to read and interpret graphical provided information. The students had to answer 9 open and 4 multiple choice questions. Each of the questions represents one of the 3 levels of graph comprehension: reading the data, reading between the data, reading beyond the data \(Galesic and Garcia-Retamero, 2011). Except for question 7 there is only one correct answer to every question. Question 7 does accept a range \(\>23 to \>25) of correct answers. Participants received one point for every correct solution, meaning they could achieve 0 to 13 points in total. Only the correct answer was taken into account and only the defined exact numbers were considered correct in open questions. There was no error margin, questionnaires including blank responses were not considered for our analysis.

=== Statistical methods
<statistical-methods>
…

```{webr-r}
#| context: interactive

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
) # Download the dataset

# Read the data
penguins = read.csv("penguins.csv")

# Scatterplot example: penguin bill length versus bill depth
ggplot2::ggplot(data = penguins, ggplot2::aes(x = bill_length_mm, y = bill_depth_mm)) + # Build a scatterplot
  ggplot2::geom_point(ggplot2::aes(color = species, 
                 shape = species),
             size = 2)  +
  ggplot2::scale_color_manual(values = c("darkorange","darkorchid","cyan4"))
```

== Results
<results>
=== Recruitment Process and Demographic Characteristics
<recruitment-process-and-demographic-characteristics>
The recruitment process is shown in Figure 1. We obtained XX complete data sets \(return rate YY.Z%) after contacting …

=== Primary and secondary Outcomes
<primary-and-secondary-outcomes>
#figure([
#box(width: 1682.0pt, image("Durchschnittswerte_Selbsteinschätzung_NKLM_14a.jpg"))
], caption: figure.caption(
position: bottom, 
[
Beispielgrafik: ein Bild sagt mehr als tausend Worte …
]), 
kind: "quarto-float-fig", 
supplement: "Figure", 
)


== Discussion
<discussion>
=== Summary
<summary>
After the evaluation of all datasets, the following findings emerged. The first is that …

=== Limitation: study population
<limitation-study-population>
Nullam dapibus cursus dolor sit amet consequat. Nulla facilisi. Curabitur vel nulla non magna lacinia tincidunt. Duis porttitor quam leo, et blandit velit efficitur ut. Etiam auctor tincidunt porttitor. Phasellus sed accumsan mi. Fusce ut erat dui. Suspendisse eu augue eget turpis condimentum finibus eu non lorem. Donec finibus eros eu ante condimentum, sed pharetra sapien sagittis. Phasellus non dolor ac ante mollis auctor nec et sapien. Pellentesque vulputate at nisi eu tincidunt. Vestibulum at dolor aliquam, hendrerit purus eu, eleifend massa. Morbi consectetur eros id tincidunt gravida. Fusce ut enim quis orci hendrerit lacinia sed vitae enim.

=== Limitation: study design
<limitation-study-design>
…

=== Integration with prior work
<integration-with-prior-work>
Only a few studies provide insights into the graphical and numerical skills among medical students.

In a cross-sectional, descriptive study, the researchers applied the Objective Numeracy, Subjective Numeracy, and Graph Literacy Scales to medical students in their final two years of medical school and to medical residents. The study included 169 participants, comprising 70% sixth-year seventh-year students, and 30% residents. The findings showed that the mean graph literacy was 10.35. A multiple linear regression analysis revealed that higher scores in the Graph Literacy Scale were associated with the male gender and younger age. The study concluded that numeracy and graph literacy scales’ mean scores were high among the medical students in this sample \[9\].

\[10\] highlights the importance of graph literacy in patient-centered communication tools and its correlation with dashboard comprehension.

\[11\] emphasizes the significance of graph literacy in understanding health-related statistical information and its relationship with comprehension and recall.

\[12\] examines the relationship between graph literacy, numeracy, health literacy, and sociodemographic characteristics in a Medicaid-eligible population.

\[13\] discusses the impact of different graphical representations on the accurate interpretation and clarity of patient-reported outcome scores in cancer patients/survivors.

=== Implications for practice
<implications-for-practice>
Praesent ornare dolor turpis, sed tincidunt nisl pretium eget. Curabitur sed iaculis ex, vitae tristique sapien. Quisque nec ex dolor. Quisque ut nisl a libero egestas molestie. Nulla vel porta nulla. Phasellus id pretium arcu. Etiam sed mi pellentesque nibh scelerisque elementum sed at urna. Ut congue molestie nibh, sit amet pretium ligula consectetur eu. Integer consectetur augue justo, at placerat erat posuere at. Ut elementum urna lectus, vitae bibendum neque pulvinar quis. Suspendisse vulputate cursus eros id maximus. Duis pulvinar facilisis massa, et condimentum est viverra congue. Curabitur ornare convallis nisl. Morbi dictum scelerisque turpis quis pellentesque. Etiam lectus risus, luctus lobortis risus ut, rutrum vulputate justo. Nulla facilisi.

=== Implications for research
<implications-for-research>
…

=== Conclusions
<conclusions>
…

#block[
#heading(
level: 
2
, 
numbering: 
none
, 
[
References
]
)
]
#block[
#block[
1. Friel SN, Curcio FR, Bright GW. Making sense of graphs: Critical factors influencing comprehension and instructional implications. Journal for Research in mathematics Education. 2001;32:124–58.

] <ref-friel2001making>
#block[
2. Cooper RJ, Schriger DL, Close RJ. Graphical literacy: The quality of graphs in a large-circulation journal. Annals of emergency medicine. 2002;40:317–22.

] <ref-cooper2002graphical>
#block[
3. Melnik-Leroy GA, Aidokas L, Dzemyda G, Dzemydaitė G, Marcinkevičius V, Tiešis V, et al. Is my visualization better than yours? Analyzing factors modulating exponential growth bias in graphs. Frontiers in Psychology. 2023;14:1125810.

] <ref-melnik2023my>
#block[
4. Roth W-M. Reading graphs: Contributions to an integrative concept of literacy. Journal of curriculum studies. 2002;34:1–24.

] <ref-roth2002reading>
#block[
5. Okan Y, Janssen E, Galesic M, Waters EA. Using the short graph literacy scale to predict precursors of health behavior change. Medical Decision Making. 2019;39:183–95.

] <ref-okan2019using>
#block[
6. Durand M-A, Yen RW, O’Malley J, Elwyn G, Mancini J. Graph literacy matters: Examining the association between graph literacy, health literacy, and numeracy in a medicaid eligible population. PloS one. 2020;15:e0241844.

] <ref-durand2020graph>
#block[
7. Caverly TJ, Prochazka AV, Combs BP, Lucas BP, Mueller SR, Kutner JS, et al. Doctors and numbers: An assessment of the critical risk interpretation test. Medical Decision Making. 2015;35:512–24.

] <ref-caverly2015doctors>
#block[
8. Galesic M, Garcia-Retamero R. Graph literacy: A cross-cultural comparison. Medical Decision Making. 2011;31:444457.

] <ref-galesic2011>
#block[
9. Mas G, Tello T, Ortiz P, Petrova D, Garcı́a-Retamero R. Graphical and numerical skills in pre-and postgraduate medical students from a private university. Gac Med Mex. 2018;154:163–9.

] <ref-mas2018graphical>
#block[
10. Nayak JG, Hartzler AL, Macleod LC, Izard JP, Dalkin BM, Gore JL. #link("https://doi.org/10.1016/j.pec.2015.09.009")[Relevance of graph literacy in the development of patient-centered communication tools];. Patient Education and Counseling. 2016;99:448–54.

] <ref-Nayak2016>
#block[
11. Gaissmaier W, Wegwarth O, Skopec D, Müller A-S, Broschinski S, Politi MC. #link("https://doi.org/10.1037/a0024850")[Numbers can be worth a thousand pictures: Individual differences in understanding graphical and numerical representations of health-related information.] Health Psychology. 2012;31:286–96.

] <ref-Gaissmaier2012>
#block[
12. Durand M-A, Yen RW, O’Malley J, Elwyn G, Mancini J. #link("https://doi.org/10.1371/journal.pone.0241844")[Graph literacy matters: Examining the association between graph literacy, health literacy, and numeracy in a Medicaid eligible population];. PLOS ONE. 2020;15:e0241844.

] <ref-Durand2020>
#block[
13. Tolbert E, Brundage M, Bantug E, Blackford AL, Smith K, Snyder C. #link("https://doi.org/10.1177/0272989x18791177")[Picture This: Presenting Longitudinal Patient-Reported Outcome Research Study Results to Patients];. Medical Decision Making. 2018;38:994–1005.

] <ref-Tolbert2018>
] <refs>
== Declarations
<declarations>
=== Ethics approval and consent to participate
<ethics-approval-and-consent-to-participate>
Vestibulum ultrices, tortor at mattis porta, odio nisi rutrum nulla, sit amet tincidunt eros quam facilisis tellus. Fusce eleifend lectus in elementum lacinia. Nam auctor nunc in massa ullamcorper, sit amet auctor ante accumsan. Nam ut varius metus. Curabitur eget tristique leo. Cras finibus euismod erat eget elementum. Integer vel placerat ex. Ut id eros quis lectus lacinia venenatis hendrerit vel ante.

=== Consent for publication
<consent-for-publication>
Not applicable

=== Availability of data and materials
<availability-of-data-and-materials>
The original data that support the findings of this study are available from Open Science Framework \(osf.io, see manuscript-URL).

=== Competing interests
<competing-interests>
The authors declare that they have no competing interests.

=== Funding
<funding>
The author\(s) received no specific funding for this work.

=== Authors’ contributions
<authors-contributions>
Duis urna urna, pellentesque eu urna ut, malesuada bibendum dolor. Suspendisse potenti. Vivamus ornare, arcu quis molestie ultrices, magna est accumsan augue, auctor vulputate erat quam quis neque. Nullam scelerisque odio vel ultricies facilisis. Ut porta arcu non magna sagittis lacinia. Cras ornare vulputate lectus a tristique. Pellentesque ac arcu congue, rhoncus mi id, dignissim ligula.

=== CRediT authorship contribution statement
<credit-authorship-contribution-statement>
#strong[Janina Soler Wenglein:] Data curation, Formal analysis, Investigation, Methodology, Visualization, Writing - original draft. \
#strong[Hendrik Friederichs:] Conceptualization, Formal analysis, Investigation, Methodology, Supervision, Writing - review & editing, Writing - original draft.

=== Acknowledgments
<acknowledgments>
The authors are grateful for the insightful comments offered by the anonymous peer reviewers at Ziel-Journal. The generosity and expertise of one and all have improved this study in innumerable ways and saved us from many errors; those that inevitably remain are entirely our own responsibility.
