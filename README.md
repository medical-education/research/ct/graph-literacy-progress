## Graph Literacy of medical students

Graph literacy, the ability to understand and interpret graphical data, is a crucial skill in the medical field. This skill is vital for various aspects such as data analysis, clinical decision-making, effective communication with patients, and ongoing professional development.

Graph literacy is not just an academic requirement but a practical necessity in the medical profession. It enables medical students and professionals to interpret and utilize data effectively, which is fundamental to providing high-quality patient care and advancing medical knowledge. Notably, existing research on graph literacy primarily focuses on patients or the capability of doctors to decipher graphical representations.

This study is designed to find out how well medical students can understand and use graphs. We aim to learn how extensive these skills are and how they grow.

01-2024